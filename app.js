window.onload = function(){
   var app = new Vue({
      el: '#app',
      data: {
         articles: [],
         selection: 'all',
         client: 'all',
         works: []

      },
      mounted() {
         if (window.location.href == 'http://localhost:8888/wordpress/blog/'){
            this.allCat();
         }

         if (window.location.href == 'http://localhost:8888/wordpress/portfolio/') {
            this.worksAll();
         }
      },
      methods: {
         selected(){
            if(this.selection == 'all'){
               this.allCat();
            } else{
               axios
                  .get(`http://localhost:8888/wordpress/wp-json/vfplugin/v1/posts/percategory`, {
                     params: {
                        'category': this.selection
                     }
                  })
                  .then(response => {
                     this.articles = response.data;
                  })
            }
            
         },

         allCat(){
            axios
               .get('http://localhost:8888/wordpress/wp-json/vfplugin/v1/posts')
               .then(response => {
                  console.log(response.data);
                  this.articles = response.data;
               })
         },

         worksAll(){
            axios
               .get('http://localhost:8888/wordpress/wp-json/vfplugin/v1/works')
               .then(response => {
                  this.works = response.data;
                  console.log(this.works);
               })
         },

         clientSelect(){
            if(this.client == 'all'){
               this.worksAll();
            } else {
               axios
                  .get('http://localhost:8888/wordpress/wp-json/vfplugin/v1/works/perclient', {
                     params: {
                        'cliente': this.client
                     }
                  })
                  .then(response => {
                     this.works = response.data;
                     console.log(this.works, 'CLIENT');
                  })
            }
            
         }
      }

   })
}
