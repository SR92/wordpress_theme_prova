<?php get_header(); ?>


<h1 class="title">PORTFOLIO</h1>

<?php
wp_nav_menu(array(
   'theme_location' => 'my-custom-menu',
   'container_class' => 'custom-menu-class'
));
?>


<select name="" id="" v-model="client" @change="clientSelect()">
   <option value="all">ALL</option>
   <?php
   if (have_posts()) {
      while (have_posts()) {
         the_post();
   ?>
         <option value="<?php echo get_post_meta($post->ID, 'cliente', true); ?>"><?php echo get_post_meta($post->ID, 'cliente', true); ?></option>


      <?php } ?>
   <?php } ?>
</select>


<?php
if (have_posts()) {
   while (have_posts()) {
      the_post();
?>
      <a href="<?php the_permalink() ?>" v-if="works.some(e => e.ID == <?php echo get_the_ID() ?>)">
         <h2><?php the_title() ?></h2>
         <?php the_content() ?>
         <?php the_excerpt() ?>
      </a>


   <?php } ?>
<?php } ?>






<?php get_footer() ?>