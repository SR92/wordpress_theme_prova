<?php get_header() ?>

<h1 class="title">DETTAGLIO</h1>

<h2><?php the_title() ?></h2>
<p><?php the_content() ?></p>
<p><?php the_excerpt() ?></p>

<table class="table table-dark table-striped table-bordered">
   <thead>
      <tr>
         <?php foreach (get_post_custom_keys($post->ID) as $key) {
            if($key != '_edit_last' && $key != '_edit_lock'){
         ?>
            <th><?php echo $key ?></th>
            <?php } ?>
         <?php } ?>
      </tr>
   </thead>
   <tbody>
      <tr>
         <?php foreach(get_post_custom($post->ID) as $i => $data) {
            if($i !='_edit_last' && $i != '_edit_lock'){
         ?>
               <?php foreach($data as $item){ ?>
                  <td><?php echo $item  ?></td>
               <?php } ?>
            <?php } ?>
         <?php } ?>
         <!-- <td><?php echo get_post_meta($post->ID, 'data', true); ?></td> -->
      </tr>
   </tbody>
</table>


<?php get_footer() ?>