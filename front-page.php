<?php get_header() ?>
<header>
   <h1 class="title"><?php the_title() ?></h1>


   <?php
   wp_nav_menu(array(
      'theme_location' => 'my-custom-menu',
      'container_class' => 'custom-menu-class'
   ));
   ?>

</header>


<p><?php the_content() ?></p>


<?php

$args = [
   'post_type' => 'post',
   'post_per_page' => 1
];
// The Query
$the_query = new WP_Query($args);

// The Loop
if ($the_query->have_posts()) {
   

   $the_query->the_post();
?>
   <h3 class="text-center">Ultimo articolo:</h3>
   <div class="box">
      <div class="article-container">
         <h2 class="article-title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
      </div>   
   </div>

<?php
} else {
   // no posts found
}
/* Restore original Post Data */
wp_reset_postdata();
?>





<?php get_footer() ?>