<?php get_header() ?>
<header>
   <h1 class="title">BLOG</h1>


   <?php
   wp_nav_menu(array(
      'theme_location' => 'my-custom-menu',
      'container_class' => 'custom-menu-class'
   ));
   ?>

</header>

<label for="">Seleziona categoria</label>
<select name="" id="" @change="selected()" v-model="selection">
   <option value="all">ALL</option>
   <?php $categories = get_category_name();
   ?>

   <?php foreach ($categories as $key => $category) {
      if ($category->name != 'Senza categoria') {
   ?>
         <option value="<?php echo $category->term_id ?>"><?php echo $category->name ?></option>
      <?php } ?>

   <?php } ?>
</select>



<div class="box">

   <?php if (have_posts()) : while (have_posts()) : the_post() ?>

         <div class="article-container" v-if="articles.some(item => item.ID == <?php echo get_the_ID() ?>)">

            <a href="<?php the_permalink() ?>"><?php the_post_thumbnail('s', ['class' => 'prova']) ?>
               <div class="text">
                  <?php the_title() ?>
               </div>
            </a>

         </div>

      <?php endwhile ?>

   <?php endif ?>


</div>

<?php get_footer() ?>