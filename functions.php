<?php

// Import CSS
function my_style(){
   wp_enqueue_style('style', get_stylesheet_uri());
}
add_action('wp_enqueue_scripts', 'my_style');

function my_script(){
   wp_enqueue_script('vue', 'https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.js');
   wp_enqueue_script('axios', 'https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js');
   wp_enqueue_script('app', get_template_directory_uri() . '/app.js');
}
add_action('wp_enqueue_scripts', 'my_script');


// Creazione navbar
function wpb_custom_new_menu()
{
   register_nav_menus(
      array(
         'my-custom-menu' => __('My Custom Menu'),
         'extra-menu' => __('Extra Menu')
      )
   );
}
add_action('init', 'wpb_custom_new_menu');

// Aggiunta classe active su elemento navbar
add_filter('nav_menu_css_class', 'special_nav_class', 10, 2);
function special_nav_class($classes, $item)
{
   if (in_array('current-menu-item', $classes)) {
      $classes[] = 'active';
   } else{
      global $post;
      if (get_post_type($post) == 'portfolio' && in_array('menu-item-25', $classes)) {
         $classes[] = 'active';
      }
   }
   
   return $classes;
}

// Creazione custom post type
function portfolio_plugin_init()
{
   // an array of labels
   $labels = array(

      'name'               => __('Portfolio', 'portfolio-plugin'),

      'singular_name'      => __('Portfolio', 'portfolio-plugin'),

      'add_new'            => __('Add Work', 'portfolio-plugin'),

      'add_new_item'       => __('Add New Work', 'portfolio-plugin'),

      'edit_item'          => __('Edit Work', 'portfolio-plugin'),

      'new_item'           => __('New Work', 'portfolio-plugin'),

      'all_items'          => __('All Works', 'portfolio-plugin'),

      'view_item'          => __('View Work', 'portfolio-plugin'),

      'search_items'       => __('Search Work', 'portfolio-plugin'),

      'not_found'          => __('Work Not found', 'portfolio-plugin'),

      'not_found_in_trash' => __('Work Not found in the trash', 'portfolio-plugin'),

   );

   // an array of arguments
   $args = array(

      'labels'             => $labels,

      'public'             => true,

      'rewrite'            => array('slug' => 'portfolio'),

      'has_archive'        => true,

      'hierarchical'       => true,

      'menu_position'      => 22,

      'menu_icon'        => 'dashicons - welcome - write - blog',

      'supports'           => array(

         'title',

         'editor',

         'thumbnail',

         'excerpt',

         'page - attributes',

         'custom-fields'

      ),


   );


   register_post_type('portfolio', $args);
}
add_action('init', 'portfolio_plugin_init');

// Attivare supporto immagine in evidenza
add_theme_support('post-thumbnails');

if(function_exists('add_image_size')){
   add_image_size('s', 300);
   add_image_size('m', 600, 600);
   add_image_size('l', 1200);
}

// Restituisce tutte le categorie presenti per metterle nelle option
function get_category_name(){
   $categories = get_categories();
   foreach ($categories as $key => $category) {
      $array_cat[] = $category;
   }
   
   return $array_cat;
}



// Api tutti gli articoli
add_action('rest_api_init', function () {
   register_rest_route('vfplugin/v1', '/posts/', array(
      'methods' => 'GET',
      'callback' => 'api_get_posts',
   ));
});

function api_get_posts($data)
{
   $all_posts = array();
   
   $posts = get_posts();

   return $posts;
}

// Api tutti gli articoli con categoria di id=5
add_action('rest_api_init', function () {
   register_rest_route('vfplugin/v1', '/posts/percategory', array(
      'methods' => 'GET',
      'callback' => 'api_get_posts_percat',
   ));
});

function api_get_posts_percat(WP_REST_Request $request)
{
   $args = [
      'category' => $request->get_param('category')
   ];
   $posts = get_posts($args);

   return $posts;
}

// Api tutti i lavori
add_action('rest_api_init', function () {
   register_rest_route('vfplugin/v1', '/works/', array(
      'methods' => 'GET',
      'callback' => 'api_get_works',
   ));
});

function api_get_works($data)
{
   $args = [
      'post_type' => 'portfolio'
   ];
   $works = new WP_Query($args);

   return $works->posts;
}

add_action('rest_api_init', function () {
   register_rest_route('vfplugin/v1', '/works/perclient', array(
      'methods' => 'GET',
      'callback' => 'api_get_works_perclient',
   ));
});

function api_get_works_perclient(WP_REST_Request $request)
{
   $args = [
      'post_type' => 'portfolio',
      'meta_key' => 'cliente',
      'meta_value' => $request->get_param('cliente')
   ];
   $works = new WP_Query($args);

   return $works->posts;
}


?>

