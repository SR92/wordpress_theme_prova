<?php get_header() ?>
      <header>
         <h1 class="title text-center"><?php the_title() ?></h1>


         <?php
         wp_nav_menu(array(
            'theme_location' => 'my-custom-menu',
            'container_class' => 'custom-menu-class'
         ));
         ?>

      </header>
      
      
      
      <p><?php the_content() ?></p>

      
      
      
   <?php get_footer() ?>